﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web;
using DotNetWindowsInfo.models;
using DotNetWindowsInfo;

namespace DotNetHttp {
    /// <summary>
    /// Http requests utilities, mainly from https://stackoverflow.com/questions/27108264/c-sharp-how-to-properly-make-a-http-web-get-request
    /// </summary>
    public class Http {

        private const string METHOD_GET = "GET";
        private const string METHOD_POST = "POST";

        private static Http mInstance;
        private uint mWinBuild;

        private Http() {
            this.mWinBuild = DotNetWindowsInfo.SystemInfo.GetInfo().CurrentBuild;
            this.prepareFramework();
        }

        public static Http getInstance() {
            return mInstance ?? (mInstance = new Http());
        }

        /// <summary>
        /// Set up the .Net framework before making requests
        /// </summary>
        private void prepareFramework() {
            if (this.mWinBuild < 8000) {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                       | SecurityProtocolType.Tls11
                                                       | SecurityProtocolType.Tls12
                                                       | SecurityProtocolType.Ssl3;
            }
        }

        /*
         * Public methods
         */

        /// <summary>
        /// Perform an asynchronous get request by passing query parameters.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="queryParameters"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(string uri, Dictionary<string, string> queryParameters) {
            var finalUri = CreateUri(uri, queryParameters);
            return await GetAsync(finalUri);
        }

        /// <summary>
        /// Perform an asynchronous get request.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(string uri) {
            return await RequestAsync(uri, "", "", METHOD_GET);
        }

        /// <summary>
        /// Do a generic http request
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public async Task<string> RequestAsync(string uri, string data, string contentType, string method) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            // Write body only if present
            if (data.Length != 0 && !method.Equals(METHOD_GET)) {
                request.Method = method;
                byte[] dataBytes = Encoding.UTF8.GetBytes(data);
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;

                using (Stream requestBody = request.GetRequestStream()) {
                    await requestBody.WriteAsync(dataBytes, 0, dataBytes.Length);
                }
            }

            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream)) {
                return await reader.ReadToEndAsync();
            }
        }

        /// <summary>
        /// Create a uri with query parameters
        /// </summary>
        /// <param name="baseUri"></param>
        /// <param name="queryParameters"></param>
        /// <returns></returns>
        public string CreateUri(string baseUri, Dictionary<string, string> queryParameters) {
            var uriBuilder = new UriBuilder(baseUri);
            var parameters = HttpUtility.ParseQueryString(string.Empty);

            foreach (string key in queryParameters.Keys) {
                parameters[key] = queryParameters[key];
            }

            uriBuilder.Query = parameters.ToString();
            Uri finalUrl = uriBuilder.Uri;

            return finalUrl.ToString();
        }

    }
}
