﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iLyricsGrabber
{
    public partial class Activation : Form
    {
        public Activation()
        {
            InitializeComponent();
        }

        private void Activation_Load(object sender, EventArgs e)
        {
            label1.Text = Properties.Settings.Default.TA.ToString() + " track(s) analyzed of " + Properties.Settings.Default.MTA.ToString() + "available for trial";
        }
    }
}
