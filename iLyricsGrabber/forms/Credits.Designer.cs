﻿namespace iLyricsGrabber
{
    partial class Credits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Credits));
            this.label2 = new System.Windows.Forms.Label();
            this.version_label_credits = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.official_site_link_label = new System.Windows.Forms.LinkLabel();
            this.official_site_label = new System.Windows.Forms.Label();
            this.bugreporting_label = new System.Windows.Forms.Label();
            this.bugreporting_link_label = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.logfile_groupBox = new System.Windows.Forms.GroupBox();
            this.logfilepath_label = new System.Windows.Forms.Label();
            this.filesize_label = new System.Windows.Forms.Label();
            this.sendtodeveloper_button = new System.Windows.Forms.Button();
            this.clear_logfile_button = new System.Windows.Forms.Button();
            this.open_logfile_button = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.myBC = new System.Windows.Forms.RichTextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.logfile_groupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // version_label_credits
            // 
            resources.ApplyResources(this.version_label_credits, "version_label_credits");
            this.version_label_credits.Name = "version_label_credits";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // official_site_link_label
            // 
            resources.ApplyResources(this.official_site_link_label, "official_site_link_label");
            this.official_site_link_label.Name = "official_site_link_label";
            this.official_site_link_label.TabStop = true;
            this.official_site_link_label.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.official_site_link_label_LinkClicked);
            // 
            // official_site_label
            // 
            resources.ApplyResources(this.official_site_label, "official_site_label");
            this.official_site_label.Name = "official_site_label";
            // 
            // bugreporting_label
            // 
            resources.ApplyResources(this.bugreporting_label, "bugreporting_label");
            this.bugreporting_label.Name = "bugreporting_label";
            // 
            // bugreporting_link_label
            // 
            resources.ApplyResources(this.bugreporting_link_label, "bugreporting_link_label");
            this.bugreporting_link_label.Name = "bugreporting_link_label";
            this.bugreporting_link_label.TabStop = true;
            this.bugreporting_link_label.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.bugreporting_link_label_LinkClicked);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // logfile_groupBox
            // 
            this.logfile_groupBox.Controls.Add(this.logfilepath_label);
            this.logfile_groupBox.Controls.Add(this.filesize_label);
            this.logfile_groupBox.Controls.Add(this.sendtodeveloper_button);
            this.logfile_groupBox.Controls.Add(this.clear_logfile_button);
            this.logfile_groupBox.Controls.Add(this.open_logfile_button);
            resources.ApplyResources(this.logfile_groupBox, "logfile_groupBox");
            this.logfile_groupBox.Name = "logfile_groupBox";
            this.logfile_groupBox.TabStop = false;
            // 
            // logfilepath_label
            // 
            resources.ApplyResources(this.logfilepath_label, "logfilepath_label");
            this.logfilepath_label.Name = "logfilepath_label";
            // 
            // filesize_label
            // 
            resources.ApplyResources(this.filesize_label, "filesize_label");
            this.filesize_label.Name = "filesize_label";
            // 
            // sendtodeveloper_button
            // 
            resources.ApplyResources(this.sendtodeveloper_button, "sendtodeveloper_button");
            this.sendtodeveloper_button.Name = "sendtodeveloper_button";
            this.sendtodeveloper_button.UseVisualStyleBackColor = true;
            this.sendtodeveloper_button.Click += new System.EventHandler(this.sendtodeveloper_button_Click);
            // 
            // clear_logfile_button
            // 
            resources.ApplyResources(this.clear_logfile_button, "clear_logfile_button");
            this.clear_logfile_button.Name = "clear_logfile_button";
            this.clear_logfile_button.UseVisualStyleBackColor = true;
            this.clear_logfile_button.Click += new System.EventHandler(this.clear_logfile_button_Click);
            // 
            // open_logfile_button
            // 
            this.open_logfile_button.BackColor = System.Drawing.Color.DarkOrange;
            resources.ApplyResources(this.open_logfile_button, "open_logfile_button");
            this.open_logfile_button.Name = "open_logfile_button";
            this.open_logfile_button.UseVisualStyleBackColor = false;
            this.open_logfile_button.Click += new System.EventHandler(this.open_logfile_button_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBox1);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // myBC
            // 
            this.myBC.BackColor = System.Drawing.SystemColors.MenuBar;
            this.myBC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.myBC, "myBC");
            this.myBC.Name = "myBC";
            this.myBC.ReadOnly = true;
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.button4, "button4");
            this.button4.Image = global::iLyricsGrabber.Properties.Resources.btn_donate_SM;
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // Credits
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.myBC);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.logfile_groupBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bugreporting_link_label);
            this.Controls.Add(this.bugreporting_label);
            this.Controls.Add(this.official_site_label);
            this.Controls.Add(this.official_site_link_label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.version_label_credits);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Credits";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Credits_Load);
            this.logfile_groupBox.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label version_label_credits;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel official_site_link_label;
        private System.Windows.Forms.Label official_site_label;
        private System.Windows.Forms.Label bugreporting_label;
        private System.Windows.Forms.LinkLabel bugreporting_link_label;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox logfile_groupBox;
        private System.Windows.Forms.Label filesize_label;
        private System.Windows.Forms.Button sendtodeveloper_button;
        private System.Windows.Forms.Button clear_logfile_button;
        private System.Windows.Forms.Button open_logfile_button;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox myBC;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label logfilepath_label;
    }
}