﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace iLyricsGrabber {
    public partial class Credits : Form {
        string logFileName = Main.LOGFILE_NAME;
        public static String BUG_REPORTING_LINK = "https://itlg.gabrielepmattia.com/bugs";
        public static String ITLG_LINK = "https://itlg.gabrielepmattia.com/";
        public static String ITLG_LICENSE_LINK = "https://bit.ly/iTLGlicense";
        public static String DONATE_LINK = "https://itlg.gabrielepmattia.com/donate";

        public Credits() {
            InitializeComponent();
        }

        private void Credits_Load(object sender, EventArgs e) {
            myBC.SelectionAlignment = HorizontalAlignment.Center;
            //   if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            //   {
            //       version_label_credits.Text = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            //   }
            FileInfo f = new FileInfo(logFileName);

            if (f.Exists == false) {
                filesize_label.Text = "";
                logfilepath_label.Text = "Logfile doesn't exist.";
                disableLogFileGroupBox();
            } else {
                logfile_groupBox.Enabled = true;
                long s1 = f.Length;
                logfilepath_label.Text = logFileName;
                filesize_label.Text = s1 / 1000 + " kb";
            }

            version_label_credits.Text = "v." + Application.ProductVersion;
            label7.Text = Properties.Settings.Default.TA.ToString() + " track(s) analyzed"; // of " + Properties.Settings.Default.MTA.ToString() + " available for trial";
        }

        private void clear_logfile_button_Click(object sender, EventArgs e) {
            FileInfo f = new FileInfo(logFileName);
            f.Delete();
            disableLogFileGroupBox();
        }

        private void sendtodeveloper_button_Click(object sender, EventArgs e) {
            Process.Start(logFileName);
            Process.Start("http://goo.gl/forms/MwPpU1mmbVtKusFI2");
        }

        private void button4_Click(object sender, EventArgs e) {
            Process.Start(DONATE_LINK);
        }

        private void official_site_link_label_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(ITLG_LINK);
        }

        private void bugreporting_link_label_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(BUG_REPORTING_LINK);
        }

        private void license_link_label_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(ITLG_LICENSE_LINK);
        }

        private void open_logfile_button_Click(object sender, EventArgs e) {
            Process.Start(logFileName);
        }

        // =================================== GUI Methods ===================================
        private void disableLogFileGroupBox() {
            logfile_groupBox.Enabled = false;
            filesize_label.Text = "n/a kb";
        }

    }
}
