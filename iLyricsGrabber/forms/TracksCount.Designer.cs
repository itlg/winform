﻿namespace iLyricsGrabber
{
    partial class TracksCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.AlbumPic_radio = new System.Windows.Forms.RadioButton();
            this.Lyrics_radio = new System.Windows.Forms.RadioButton();
            this.AlbumName_radio = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.number_col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title_col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artist_col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Album_col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count_button = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "This tool allows you to count and list the tracks without:";
            // 
            // AlbumPic_radio
            // 
            this.AlbumPic_radio.AutoSize = true;
            this.AlbumPic_radio.Location = new System.Drawing.Point(279, 7);
            this.AlbumPic_radio.Name = "AlbumPic_radio";
            this.AlbumPic_radio.Size = new System.Drawing.Size(90, 17);
            this.AlbumPic_radio.TabIndex = 1;
            this.AlbumPic_radio.TabStop = true;
            this.AlbumPic_radio.Text = "Album Picture";
            this.AlbumPic_radio.UseVisualStyleBackColor = true;
            // 
            // Lyrics_radio
            // 
            this.Lyrics_radio.AutoSize = true;
            this.Lyrics_radio.Checked = true;
            this.Lyrics_radio.Location = new System.Drawing.Point(370, 7);
            this.Lyrics_radio.Name = "Lyrics_radio";
            this.Lyrics_radio.Size = new System.Drawing.Size(52, 17);
            this.Lyrics_radio.TabIndex = 2;
            this.Lyrics_radio.TabStop = true;
            this.Lyrics_radio.Text = "Lyrics";
            this.Lyrics_radio.UseVisualStyleBackColor = true;
            // 
            // AlbumName_radio
            // 
            this.AlbumName_radio.AutoSize = true;
            this.AlbumName_radio.Location = new System.Drawing.Point(428, 7);
            this.AlbumName_radio.Name = "AlbumName_radio";
            this.AlbumName_radio.Size = new System.Drawing.Size(85, 17);
            this.AlbumName_radio.TabIndex = 3;
            this.AlbumName_radio.Text = "Album Name";
            this.AlbumName_radio.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.number_col,
            this.Title_col,
            this.Artist_col,
            this.Album_col});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(15, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(498, 295);
            this.dataGridView1.TabIndex = 4;
            // 
            // number_col
            // 
            this.number_col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.number_col.HeaderText = "N.";
            this.number_col.Name = "number_col";
            this.number_col.ReadOnly = true;
            this.number_col.Width = 43;
            // 
            // Title_col
            // 
            this.Title_col.HeaderText = "Title";
            this.Title_col.Name = "Title_col";
            this.Title_col.ReadOnly = true;
            // 
            // Artist_col
            // 
            this.Artist_col.HeaderText = "Artist";
            this.Artist_col.Name = "Artist_col";
            this.Artist_col.ReadOnly = true;
            // 
            // Album_col
            // 
            this.Album_col.HeaderText = "Album";
            this.Album_col.Name = "Album_col";
            this.Album_col.ReadOnly = true;
            // 
            // count_button
            // 
            this.count_button.Location = new System.Drawing.Point(370, 331);
            this.count_button.Name = "count_button";
            this.count_button.Size = new System.Drawing.Size(143, 45);
            this.count_button.TabIndex = 5;
            this.count_button.Text = "Count";
            this.count_button.UseVisualStyleBackColor = true;
            this.count_button.Click += new System.EventHandler(this.count_button_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(15, 347);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(349, 28);
            this.progressBar1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(12, 331);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ready!";
            // 
            // TracksCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 386);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.count_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.AlbumName_radio);
            this.Controls.Add(this.Lyrics_radio);
            this.Controls.Add(this.AlbumPic_radio);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TracksCount";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Track Count";
            this.Load += new System.EventHandler(this.TracksCount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton AlbumPic_radio;
        private System.Windows.Forms.RadioButton Lyrics_radio;
        private System.Windows.Forms.RadioButton AlbumName_radio;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button count_button;
        private System.Windows.Forms.DataGridViewTextBoxColumn number_col;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title_col;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist_col;
        private System.Windows.Forms.DataGridViewTextBoxColumn Album_col;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label2;
    }
}