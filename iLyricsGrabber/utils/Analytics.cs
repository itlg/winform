﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetAnalytics;

namespace iLyricsGrabber.utils {
    class Analytics {
        private const string ACTION_CATEGORY_USER = "user";
        private const string ACTION_CATEGORY_APP = "app";

        private const string ACTION_STARTED_APP_NAME = "started-app";
        private const string ACTION_PRESSED_NAME = "button-pressed";
        private const string ACTION_PRESSED_GRAB_LABEL = "grab";
        private const string ACTION_DOWNLOAD_LYRICS_NAME = "download-lyrics";
        private const string ACTION_DOWNLOAD_LYRICS_STARTED_LABEL = "started";
        private const string ACTION_DOWNLOAD_LYRICS_SUCCESS_LABEL = "success";
        private const string ACTION_DOWNLOAD_LYRICS_ERROR_LABEL = "error";

        private const string EXCEPTION_PREFIX = "EX";

        private GoogleAnalyticsManager googleAnalyticsManager;
        private Analytics(string uaid, string clientId) {
            this.googleAnalyticsManager = new GoogleAnalyticsManager(uaid, clientId);
        }

        private bool IsAnalyticsEnabled() {
            return Properties.Settings.Default.SettingAnalytics;
        }

        /*
         * Exported
         */

        public async void StartedApplication(string appVersion) {
            await googleAnalyticsManager.LogEvent(ACTION_CATEGORY_USER, ACTION_STARTED_APP_NAME, appVersion, 0);
        }

        public async void GrabPressed() {
            await googleAnalyticsManager.LogEvent(ACTION_CATEGORY_USER, ACTION_PRESSED_NAME, ACTION_PRESSED_GRAB_LABEL, 0);
        }

        public async void DownloadLyricsStarted() {
            await googleAnalyticsManager.LogEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_STARTED_LABEL, 0);
        }

        public async void DownloadLyricsError() {
            await googleAnalyticsManager.LogEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_ERROR_LABEL, 0);
        }

        public async void DownloadLyricsSuccess() {
            await googleAnalyticsManager.LogEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_SUCCESS_LABEL, 0);
        }

        public async void Exception(int code) {
            await googleAnalyticsManager.LogException(EXCEPTION_PREFIX + code, false);
        }

        /*
         * Static
         */

        private static Analytics instance = null;
        public static Analytics GetInstance() {
            if (instance == null) {
                if (Properties.Settings.Default.ClientId == "") {
                    Properties.Settings.Default.ClientId = System.Guid.NewGuid().ToString();
                    Properties.Settings.Default.Save();
                }

                instance = new Analytics(Properties.Settings.Default.GAnalyticsPropertyId, Properties.Settings.Default.ClientId);
            }
            return instance;
        }
    }
}
