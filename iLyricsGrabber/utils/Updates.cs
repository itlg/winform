﻿using DotNetHttp;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iLyricsGrabber.utils {
    static class Updates {

        public class UpdateInfo {
            public string version { get; set; }
            public string url { get; set; }
        }

        public static async Task<UpdateInfo> GetUpdateInfo() {
            var jsonUpdate = await Http.getInstance().GetAsync(Properties.Settings.Default.UpdatesStableJsonUrl);
            UpdateInfo updateInfo;
            try {
                updateInfo = JsonConvert.DeserializeObject<UpdateInfo>(jsonUpdate);
            } catch (Exception) {
                return null;
            }
            return updateInfo;
        }

        public static async void CheckUpdatesAndShowDialogIfAny() {
            UpdateInfo updateInfo = await GetUpdateInfo();
            if (updateInfo != null && updateInfo.version != Application.ProductVersion && updateInfo.version != "") {
                DialogResult res = MessageBox.Show("New version " + updateInfo.version + " is available to download from the official website.\n" +
                                                   "New versions can include fixes and new features.\n\n" +
                                                   "Do you want to download it now?", "Updates available",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes) Process.Start(updateInfo.url);
            }
        }
    }

}